#!/bin/bash

# get current directory and cd to it
DIR="$(dirname "$0")"
cd $DIR

# to make directory
mkdir -p logs/bookinglogs-emails

# to make the log file
cd logs/bookinglogs-emails
# with file name of current date
DATE="$(date +%F)";
touch "${DATE}.log"

echo "==================================================="
echo "`date +%c` : Starting work"
CT="Content-Type:application/json"
TEST="curl -d '' http://172.16.4.156:3111/hatdog"
echo $TEST
RESPONSE=`$TEST`
echo $RESPONSE >> "${DATE}.log"
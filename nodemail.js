require("dotenv").config();
const nodemailer = require("nodemailer");
let transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.EMAIL_PORT, 
    useProxy: true,
    secure: true,
    debug: true,
    logger: true,
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    },
    tls: {
        rejectUnauthorized: false,
    }
});

async function sendEmail(mailOptions) {
    console.log(transporter)
    try {

        await new Promise((resolve) => {
            return transporter.sendMail(mailOptions, (err, mail) => {
                let msg;
                console.log(mailOptions)
                if (err) {
                    msg = {
                        status: 400,
                        data: `not sent\n\n${err}`,
                    }
                    console.log('datwatyougit', err)
                    resolve(msg)
                }
                else {
                    msg = {
                        status: 200,
                        data: `sent successpul\n\n${mail}`,
                    }
                    console.log('nusir', mail)

                    resolve(msg)
                }
            })
        })

    } catch (error) {
        console.log(error)
        return error
    }
}


module.exports = { sendEmail };
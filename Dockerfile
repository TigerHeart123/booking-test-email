FROM node:10 
WORKDIR app.js
COPY package*.json ./


RUN npm config set registry http://registry.npmjs.org/


RUN npm install 

CMD [ "npm", "start" ]